<?php

namespace app\controllers;

use app\models\Navlinks;

class BaseController extends \lithium\action\Controller
{
  /**
   * {@inheritdoc}
   */
  public function render(array $options=array())
  {
    $navlinks = Navlinks::find('all');
    $this->set(compact('navlinks'));
    $this->set($options);
    parent::render($options);
  }
}
