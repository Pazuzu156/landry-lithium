<?php

namespace app\controllers;

use lithium\util\Validator;

class HomeController extends BaseController
{
  public function index()
  {
    return $this->render(array(
      'title' => 'Home',
    ));
  }

  public function forms()
  {
    $request = $this->request;

    if($request->is('post'))
    {
      if(empty($request->data['test']))
        $this->set(array('emsg' => 'The field cannot be empty!'));
      else
        $this->set(array('smsg' => 'Success. Your data: ' . $request->data['test']));
    }

    return $this->render(array(
      'title' => 'Forms Test',
      'isPost' => $request->is('post'),
    ));
  }
}
