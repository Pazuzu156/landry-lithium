<div class="well">
  <?=$this->form->create(null, array('class' => 'form-horizontal'))?>
  <fieldset>
    <legend>Test Form</legend>
    <?php if($isPost): ?>
      <div class="alert alert-<?=(isset($smsg)) ? 'success' : 'danger'?> alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button><?=(isset($smsg)) ? $smsg : $emsg?></div>
    <?php endif; ?>
    <div class="form-group">
      <?=$this->form->label('test', 'Type some text:', array('class' => 'col-lg-2'))?>
      <div class="col-lg-10">
        <?=$this->form->text('test', array('class' => 'form-control'))?>
      </div>
    </div>
    <?=$this->form->submit('Test it Out!', array('class' => 'btn btn-primary'))?>
  </fieldset>
  <?=$this->form->end()?>
</div>
