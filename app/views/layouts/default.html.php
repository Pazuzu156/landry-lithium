<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title>Mr. Landry's Website &bull; <?=$title?></title>
	<?=$this->html->style(array('theme.min', 'site', 'animate.min'))?>
	<?=$this->html->script(array('global.min'))?>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div class="navbar-inner">
			<div class="container">
				<!-- <a class="navbar-brand" href="/~rlandry">Mr. Landry's Website</a> -->
				<?=$this->html->link('Mr. Landry\'s Website', '/', array('class' => 'navbar-brand'))?>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Mobile Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
					<ul class="nav navbar-nav navbar-right">
						<?=$this->_render('element', 'navbar')?>
					</ul>
				</nav>
			</div>
		</div>
	</nav>
	<div class="pageContent">
		<div class="container">
			<?=$this->content();?>
		</div>
	</div>
	<div class="footer navbar-inverse">
		<div class="container">
			<div class="footer_text">
				Site &copy; <span class="date"></span> <a href="http://kalebklein.com" target="_blank">Kaleb Klein</a>. | <a href="http://validator.w3.org/check?uri=referer" target="_blank">Valid HTML5</a> | Templating Engine v1.0
			</div>
		</div>
	</div>
</body>
</html>
