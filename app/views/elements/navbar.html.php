<?php

/**
 * This isn't so much a view as just a PHP script
 * This takes navlinks obtained from the database
 * within the controller, and generates the links
 * in the navbar accordingly. Allows for normal links,
 * dropdowns (based on parent/child relationships), and
 * for dividers and dropdown headers (dividers with content according to db)
 *
 * Also with support for opening links in new tabs if required (denoted in db)
 */

// Loop through all navlinks (Used as parents/normal links)
for($i = 1; $i <= count($navlinks); $i++)
{
	$link = $navlinks[$i];

	// Normal links
	// Denoted as a links with no children or parents
	if(!$link->has_child && $link->parent == 0)
    echo renderLink($link->content, $link->url, $this, $link->newtab);

	// Parent links
	elseif($link->has_child
		&& $link->parent == 0)
	{
		echo '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'
			. $link->content . ' <span class="caret"></span></a>'
			. "<ul class=\"dropdown-menu\">";

		// Loop through all links again, but this time, they're children
		for($c = 1; $c <= count($navlinks); $c++)
		{
			$child = $navlinks[$c];

			// Children must have a parent's id
			// Make sure the child to render matches the
			// parent's id with it's supplied parent id
			// This also is a link, not a divider!
			if($child->parent == $link->id && !$child->divider)
        echo renderLink($child->content, $child->url, $this, $child->newtab);

			// Same as if statement above, only this time
			// we'll render out the dividers. Both divider
			// and header.
			// Difference denoted by whether or not divider
			// has content (Text to display)
			elseif($child->divider && $child->parent == $link->id)
			{
				if(empty($child->content))
				{
					echo "<li role=\"presentation\" class=\"divider\"></li>";
				}
				else
				{
					echo "<li role=\"presentation\" class=\"dropdown-header\">{$child->content}</li>";
				}
			}
		}
		echo "</ul></li>";
	}
}

/**
 * Checks if a string starts with a given delemeter
 *
 * @param string $needle - The string to search for
 * @param string $haystack - The string to search in
 * @return boolean
 */
function startsWith($needle, $haystack)
{
	return (substr($haystack, 0, strlen($needle)) === $needle);
}

/**
 * Renders a link for the navbar
 *
 * @param string $content - The test of the link
 * @param string $url - The URL of the link
 * @param boolean $newtab - If the link should open in a new tab or not
 * @return string
 */
function renderLink($content, $url, $par, $newtab=false)
{
  if(startsWith('mailto:', $url))
		return "<li>".$par->html->link($content, $url)."</li>";
  else
  {
		if($newtab)
			return "<li>".$par->html->link($content, $url, array('target' => '_blank'))."</li>";
		else
			return "<li>".$par->html->link($content, $url)."</li>";
	}
}
